# Butterflies
## For Kinect Azure

---

### QuickStart

Play Scenes > Butterflies.unity to load json from path in "json/config.json."  Check for errors. With play window focused, Press and hold "1" to check if animations are working correctly. 

---


### Debug keyboard controls

**PRESS 1**:  body is present - disperse butterflies.

**RELEASE 1**  body has left - butterflies will eventually land

**PRESS 2** impatient? force sustain mode. 

**d** toggle debug mode:  this will hide all butterflies and reveal the landing points for adjustment.  Rollover the landing points and they should turn green and become draggable.  ( soon to show kinect skeleton as well )  toggling off will land all butterflies to new rock formation, but **will not save these new points** unless you press Enter or "Save Json"

**k** toggle kinect mode off. You will still be able to see kinect skeleton in debug mode, but can only trigger the flight by holding down '1' 

**ENTER** If you like the current state, press Enter to bring up save dialog window. Same as pressing "save json in editor. This will save landing point positions and all settings in ButterflyController gameObject. 

**o** toggle outline effect

**m** toggle magic mode effect.

**SPACE** resets simulation and reloads json. 

--- 

### ButterflyController 
#### Scenes > Butterflies > ButterflyController

**Load JSON** Populates the ButterflyController ( and landing points ) with the json config, the path of which is references in "json/config.json"

**Save JSON**  Brings up a dialog window to save json file. Hitting enter during playmode also brings up this dialog. Will save landing points and all current settings shown in butterflyController. Defaults to timestamp. Can save as a copy or overwrite path inside of "json/config.json". This way, multiple presets can be saved.   Check that your json file isn't filling up with junk preset files if you've hit enter a bunch. 

**Butterfly Models** Possible prefabs to Instantiate. Pull from *Assets/Prefabs* folder within Unity Project Window.  (They need butterfly script attached and referencing children) These can be weighted by bringing in duplicates. 

**showOutline** shows white outline around butterflies if true.  

**lineThickness**  thickness of line to put around butterflies. 

**magicMode** shows magic particles in build.

**butterflyScale** Scale of butterfly. Default is 1.

**numButterflies** Number of instances in pool. Default is 30. 

**flightSpeed** Global speed of butterfly movement in space. Default is 1. ( internal random variants )

**flappingSpeed** Global speed of butterfly windspeed.   Default is 1 (internal random variants and start times )

**delayToReturn** Seconds to wait before flying back to origin (has an additional random milliseconds ) Default is 1.

**sustainMode** has a sustain mode. 

**disperseDestination** and **disperseDestination2** provide a range of random destinations for butterflies once they are dispersed.

**sustainDelay** Time until butterflies go into sustain mode, making circle at top of screen. Default 6 seconds. 

**shyChance** Chance for each butterfly to have an extra delay on 1-10 seconds before coming back. (think of percentage of stragglers. Increases realism feel of testy butterflies ) Default is 0.5.  

**randomFlightChance** Default is 0.003  Chance per second for a butterfly to be individually distrubed.  Keep this pretty low since it's per second. 

**numFlybyButterflies** Default is 40.  This is the pool of flyby butterflies. Probably don't want to change this

**flybyChance** Default is 0.003  Per second chance for a butterfly to start it's flyby trajectory. 

**kinectPosition**  For manually adjusting the position of the kinect sensor. 

**kinectRotation**   Does the kinect need to be installed vertically?  Rotate the "z" axis to either 90 or 270 for a vertical orientation of the kinect. Otherwise, should leave this alone. 

**kinectScale**  For changing the scale of the skeletons relative to the display. 

**logoScale** Float for scale of logo. Make zero to hide.  Default is 2.25;

**logoPosition** Position for logo. Probably only y needs to be edited. 


---

### Config Files


**json/config.json** The path to the actual config. This is so you can swap preset files out. 
**json/butterfly.json** A provided butterfly config file with defaults. 

On scene load, anything written in the config file that differs from the default will populate the parameters of the butterfly config.   During pause, some of these values can be tweaked in editor, but reliable changes should be made  

**landingPoints**  Probably should only be adjusted manually in playmode and then saved.  Check the **json/butterfly_example.json** for all you need if you want to edit them manually. 

**screenWidth** and **screenHeight** are in config file so you can debug within the .exe without rebuilding. 



---


### Mask/Vignette

The build will create a vignette out of the alpha channel of a png file in **/masks/mask.png**  Make a backup if you are creating a new one.   Solid color will clear the vignette. 

---


### Kinect 

Kinect should work for one single person. If you're in a position where the kinect is getting in the way of testing out settings, check off "kinectMode" on the ButterflyConfig. 

*Scene > KinectStuff > KinectSensor*  location of kinect sensor in relation to camera. Should edit in the editor view. The location of this will be saved in the butterfly config json as *kinectLocation*

*Scene > KinectStuff > KinectGuide*  The position of this can't be changed, but you can scale it up and down to adjust human size. This scale will be saved in the butterfly config json as *kinectScale*




---

#### TODO



#### Completed

* ~~Logo position implemented in json~~
* ~~Logo scale implemented in json~~
* ~~Changed butterfly colors to match color scheme~~
* ~~Logo asset added~~
* ~~Changes based on feedback~~
* ~~Rotation of sensor in butterflyConfig~~ 
* ~~Orientation of butterflies matches trajectory of flight pattern so no backwards butterflies.
* ~~Kinect mode toggle 'k' for testing settings without setting off camera.
* ~~Force vertical fullscreen display~~
* ~~Kinect box skeleton in debug mode.~~
* ~~Kinect is implemented.~~
* ~~Save works on executable version.~~
* ~~Play/Build mode interface of mapping the rock point.~~
* ~~Debug Mode toggle with "d"~~
* ~~Button in editor will "update" values to match manually loaded json.~~ 
* ~~Button in editor that will "save" json files of coordinate points into a json file.~~
* ~~Can toggle magic mode.~~
* ~~Importing parameters from JSON config files.~~
* ~~Random passerby butterflies.~~
* ~~Random rare flight from landed butterfly.~~
* ~~Automatically order points from left to right so weird shapes aren't created.~~
* ~~Sustained Present Mode, circle at top.~~
* ~~Random wing movement~~ 
* ~~Dispersion animation~~
* ~~Transition between idle wing movement and flapping wing movement~~
* ~~Dynamic Landing Points~~
* ~~Random Points between Landing points~~
* ~~NumButterflies~~
* ~~Butterfly Scale~~
* ~~Toggle Outline~~

#### Backlog 

* Does spout need to be implemented on camera ?
* Make a stripped down Vector3 class that doesn't have default recursive normalized values.  
* Position based disrupter with mouse with a radius of disruption. 
* More realistic wing animations by adding more geometry and blendshapes. 



