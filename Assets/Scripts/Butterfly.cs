using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

namespace Butterflies
{
    public class Butterfly : MonoBehaviour
    {
        public int i;
        public bool flyby; //does not respond to user interaction, is just part of the background.
        public GameObject butterfly_body;
        public float speed = 1;
        public float flapping_speed = 1;
        public Vector3 origin;
        public float delayToReturn = 0;
        public float scale = 1;
        public float shyChance;
        public float randomFlightChance = 0.01f;
        public float flybyChance = 0.2f;
        public Vector3 disperseDestination;
        public Vector3 disperseDestination2;
        private bool flying;
        private bool random;
        public Animator animator;
        private Tween rotationTween;
        private Tween positionTween;
        private Tween randomPositionTween; //for the oddballs 
        private bool useDOTween;
        public GameObject magicParticles;
        private GameObject particlesInstance;
        [HideInInspector]
        public bool magicMode;
        private Vector3 prevPosition;
        private Vector3 position;
        private Vector3 velocity;


        public void Start()
        {
            reset();
            transform.position = origin;
            particlesInstance = attachMagicParticles(magicParticles, butterfly_body);
            positionTween = transform.DOMove(origin, 0); //instantiating positionTween.
            randomPositionTween = transform.DOMove(origin, 0);
            animator.Play("Idle", 0, 0f); //resetting animator butterfly position.
            animator.Play("default", 1, 0f); //resetting animator butterfly wing animation.
            animator.SetFloat("offset", Random.Range(0.0f, 1.0f)); //prevents sync in animations. 
            animator.SetFloat("flying_speed", Random.Range(0.5f, 0.7f) * speed); //this is for the figure 8
            setRandomFlappingSpeed();
            startRandomDisperse();
            if (flyby)
            {
                startRandomFlyby();
            }
            else
            {
                startRandomDisperse();
            }
            prevPosition = butterfly_body.transform.position;
            position = butterfly_body.transform.position;
        }

        public void Update()
        {
            position = butterfly_body.transform.position;
            velocity = (position - prevPosition).normalized;
            prevPosition = position;
        }

        public void reset()
        {
            transform.position = origin;
            float realScale = getScale(scale);
            butterfly_body.transform.localScale = new Vector3(realScale, realScale, realScale);
            setInitialParentRandomRotation();
            setInitialBodyRandomRotation();
        }

        private GameObject attachMagicParticles(GameObject particlesPrefab, GameObject butterfly_body)
        {
            particlesInstance = Instantiate(particlesPrefab) as GameObject;
            particlesInstance.transform.SetParent(butterfly_body.transform, false);
            setMagicMode(magicMode);
            return particlesInstance;
        }

        public void setMagicMode(bool _magicMode)
        {
            magicMode = _magicMode;
            if (particlesInstance)
            {
                particlesInstance.SetActive(magicMode);

            }
        }

        private void startRandomDisperse()
        {
            float randomDisperseDelay = Random.Range(0.8f, 1.2f); //aproximately a second. 
            InvokeRepeating("RandomDisperseCheck", 0.0f, randomDisperseDelay);
        }

        private void startRandomFlyby()
        {
            float randomFlybyDelay = Random.Range(0.8f, 1.2f);
            InvokeRepeating("RandomFlybyCheck", 0.0f, randomFlybyDelay);
        }

        private void RandomDisperseCheck()
        {
            if (!flying && !flyby && chance(randomFlightChance))
            {
                flying = true;
                flyOffScreen(true); //return
            }
        }

        private void RandomFlybyCheck()
        {
            if (chance(flybyChance))
            {
                bool alreadyPlaying = animator.GetCurrentAnimatorStateInfo(0).IsName("flyby");
                if (!alreadyPlaying)
                {
                    transform.rotation = Quaternion.identity; //big enough path we need to reset. 
                    transform.position = getRandomFlybyOrigin();
                    string flybyClip = "flyby";
                    animator.Play(flybyClip, -1, 0);
                    rotateBodyRandomly();
                    setRandomFlappingSpeed();
                    animator.SetBool("flapping", true);
                }
            }
        }



        public Vector3 getRandomFlybyOrigin()
        {
            return new Vector3(
                Random.Range(-5.5f, -11.0f),
                Random.Range(1.5f, 5.0f),
                1.0f
            );
        }

        public float getScale(float scale)
        {
            return scale * Random.Range(0.28f, 0.32f);
        }

        public void setInitialParentRandomRotation()
        {
            transform.localRotation = Quaternion.Euler(getRandomRotation(10.0f));
        }

        public void setInitialBodyRandomRotation()
        {
            butterfly_body.transform.localRotation = Quaternion.Euler(getRandomRotation(10.0f));
        }

        public Vector3 getRandomRotation(float xMax)
        {
            float y = Random.Range(30.0f, 150.0f);
            if (velocity.x < 0)
            {
                y = Random.Range(-30.0f, -150.0f);
            }
            float x = Random.Range(-xMax, xMax);
            Quaternion newRotation = Quaternion.Euler(x, y, 0);
            return newRotation.eulerAngles;
        }

        public void setRandomFlappingSpeed()
        {
            animator.SetFloat("flapping_speed", Random.Range(4, 7) * flapping_speed); // for flapping wings
        }

        public void disperse()
        {
            flying = true;
            flyOffScreen(false);
        }

        public void rotateBodyRandomly()
        {
            setRandomFlappingSpeed();
            rotationTween.Kill();
            float randomTime = Random.Range(0.6f, 2.0f);

            Vector3 newRandomRotation = new Vector3(0, 90, 0);
            if (velocity.x < 0)
            {
                newRandomRotation = new Vector3(0, -90, 0);
            }

            rotationTween = butterfly_body.transform.DORotateQuaternion(
                    Quaternion.Euler(getRandomRotation(10.0f)), randomTime)
                .OnComplete(() =>
                {
                    if (flying || flyby)
                    {
                        rotateBodyRandomly();
                    }
                })
                .SetEase(Ease.InOutSine);
        }

        public void land()
        {
            flying = false;
            flyToOrigin();
        }

        public void startSustainMode()
        {
            animator.SetBool("sustain", true);
            animator.SetBool("disperse", true);
            animator.SetBool("flapping", true);
            flyToCenterTop();
        }

        public void flyToCenterTop()
        {
            Vector3 destination = new Vector3(
                0,
                Random.Range(1.5f, 1.5f),
                0
            );
            float randomTime = Random.Range(4.0f, 8.0f);
            positionTween.Kill();
            positionTween = transform.DOMove(destination, randomTime)
                .SetEase(Ease.InOutSine);
        }

        private void flyOffScreen(bool returnToOrigin)
        {
            float flyOffScreenTime = 0.5f / speed;
            Vector3 destination = getDisperseDestination();
            float distance = Vector3.Distance(transform.position, destination);
            float animationTime = Mathf.Max(distance * flyOffScreenTime, Random.Range(2.0f, 3.0f)) + Random.Range(0.0f, 0.5f);
            float randomDelay = Random.Range(0.0f, 0.2f);
            positionTween.Kill();
            positionTween = transform.DOMove(destination, animationTime)
                .OnStart(() =>
                {
                    animator.SetBool("disperse", true);
                    animator.SetBool("flapping", true);
                    rotateBodyRandomly();
                })
                .OnComplete(() =>
                {
                    if (returnToOrigin)
                    {
                        flyToOrigin();
                    }
                })
                .SetEase(Ease.InSine)
                .SetDelay(randomDelay);
        }

        private Vector3 getDisperseDestination(){
            return new Vector3(
                Random.Range(disperseDestination.x, disperseDestination2.x),
                Random.Range(disperseDestination.y, disperseDestination2.y),
                Random.Range(disperseDestination.z, disperseDestination2.z)
            );
        }

        private void flyToOrigin()
        {
            animator.SetBool("sustain", false);
            float returnHomeTime = 2.0f / speed;
            float distance = Vector3.Distance(transform.position, origin);
            float animationTime = Mathf.Max(distance * returnHomeTime, Random.Range(2.0f, 3.0f)) + Random.Range(0.0f, 0.3f);
            float delay = delayToReturn + Random.Range(0.0f, 2.0f);
            positionTween.Kill();
            randomPositionTween.Kill();
            if (chance(shyChance) && distance > 1.0f)
            {
                delay = delayToReturn + Random.Range(5.0f, 10.0f);
                randomPositionTween = transform.DOMove(
                    new Vector3(
                        Random.Range(-4.5f, 4.5f),
                        Random.Range(1.5f, 7.5f),
                        0
                    ),
                    delay
                ).SetEase(Ease.InOutSine);
            }
            positionTween = transform.DOMove(origin, animationTime)
                .OnStart(() =>
                {
                    animator.SetBool("disperse", false);
                    rotationTween.Kill();
                    rotationTween = butterfly_body.transform.DORotate(
                            getRandomRotation(10.0f), animationTime)
                        .SetEase(Ease.InOutSine)
                        .SetDelay(delayToReturn);
                })
                .OnComplete(() =>
                {
                    animator.SetBool("flapping", false);
                })
                .SetEase(Ease.InOutSine)
                .SetDelay(delay);
        }

        private bool chance(float amount)
        {
            return Random.Range(0.0f, 1.0f) < amount;
        }
    }
}