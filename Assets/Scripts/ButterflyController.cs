using System.Collections.Generic;
using System.IO;
using DG.Tweening;
using Newtonsoft.Json;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;
using UnityEngine.SceneManagement;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Butterflies {
    public class ButterflyController : MonoBehaviour {
        public List<GameObject> butterflyModels;
        public bool showOutline = false;
        public bool magicMode = false;
        public float butterflyScale = 1.0f;
        public int numButterflies = 40;
        public float flightSpeed = 1.0f;
        public float flappingSpeed = 1.0f;
        public float delayToReturn = 0.0f;
        public bool sustainMode;
        public float sustainDelay = 10.0f; // butterflies warm up to user and fly in circles. 
        public float shyChance = 0.1f; //chance of being shy, extra slow to return. 
        public float randomFlightChance = 0.1f;
        public int numFlybyButterflies = 20;
        public float flybyChance = 0.1f;
        public bool kinectMode = false;
        public GameObject logo;
        public Vector3 disperseDestination;
        public Vector3 disperseDestination2;
        public Camera cam;
        public KinectCollider kinectColliderScript;
        private int instanceI;
        private int landingPointI; //temporary, replace with landing point calculation. 
        private List<Butterfly> butterflies;
        private List<Butterfly> flybyButterflies;
        private bool landingPointVisibility = false;
        private cakeslice.OutlineEffect outlineEffect;
        private float startedFlyingTime;
        private bool sustained;
        private bool landing;
        private bool debugMode;
        [HideInInspector]
        public ButterflyConfig butterflyConfig;
        private List<Renderer> kinectRenderers;
        private List<Renderer> allRenderers; //cached list of visible frontend objects. 
        private List<Vector3> landingPoints;
        private List<GameObject> draggablePoints;
        public GameObject kinectGuide;
        public GameObject kinectSensor;
        public GameObject landingPointPrefab;
        public Collider kinectCollider; //need to turn off on debug mode. 
        public PostProcessVolume postProcessVolume;
        private Vignette vignetteLayer = null;

        private void Start () {
            landing = true; //prevents sustain from firing. 
            setupOutline ();
            loadConfig ();
            setupDraggablePoints ();
            allRenderers = new List<Renderer> ();
            setupButterflyInstances ();
            setupFlybyButterflyInstances ();
            setupTweens ();
            setupKinectGuide ();
            turnOffDebugMode ();
            setupPostProcessing ();
        }

        private void setupPostProcessing () {
            if (postProcessVolume != null) {
                postProcessVolume.profile.TryGetSettings (out vignetteLayer);
                Texture2D texture = Tools.LoadImage ("masks/mask.png");
                vignetteLayer.mask.value = texture;
            }
        }

        private void Update () {
            checkKeyDown ();
            checkKeyUp ();
            checkSustainedPresence ();
        }

        public void disperse () {
            if (!sustained) {
                foreach (Butterfly butterfly in butterflies) {
                    butterfly.disperse ();
                }
                landing = false;
                startedFlyingTime = Time.time;
            }
        }

        public void land () {
            sustained = false;
            landing = true;
            foreach (Butterfly butterfly in butterflies) {
                butterfly.land ();
            }
        }

        private void setupTweens () {

            DOTween.SetTweensCapacity (2000, 500);
        }

        public void loadConfig () {
            butterflyConfig = loadButterflyConfig ();
            Screen.SetResolution (butterflyConfig.screenWidth, butterflyConfig.screenHeight, true);
#if !UNITY_EDITOR
            Cursor.visible = false;
#endif
            applyConfigValues (butterflyConfig);
            sortLandingPoints ();
        }

        private ButterflyConfig loadButterflyConfig () {
            string configJSON = Tools.getJSON ("json/config.json");
            Config config = JsonConvert.DeserializeObject<Config> (configJSON);
            if (config != null && config.config != null) {
                string butterflyJSON = Tools.getJSON ("json/" + config.config);
                if (string.IsNullOrEmpty (butterflyJSON)) {
                    Debug.Log ("Could not find butterfly config file. Check json/config.json");
                    butterflyJSON = Tools.getJSON ("json/butterfly.json");
                }
                ButterflyConfig butterflyConfig = JsonConvert.DeserializeObject<ButterflyConfig> (butterflyJSON);
                return butterflyConfig;
            } else {
                Debug.Log ("Could not load config.json.");
            }
            return null;
        }

        public ButterflyConfig getButterflyConfig () {
            //generate config values for saving to json. 
            ButterflyConfig newButterflyConfig = new ButterflyConfig ();
            newButterflyConfig.showOutline = showOutline;
            newButterflyConfig.lineThickness = outlineEffect.lineThickness;
            newButterflyConfig.magicMode = magicMode;
            newButterflyConfig.butterflyScale = butterflyScale;
            newButterflyConfig.numButterflies = numButterflies;
            newButterflyConfig.flightSpeed = flightSpeed;
            newButterflyConfig.flappingSpeed = flappingSpeed;
            newButterflyConfig.delayToReturn = delayToReturn;
            newButterflyConfig.sustainMode = sustainMode;
            newButterflyConfig.sustainDelay = sustainDelay;
            newButterflyConfig.shyChance = shyChance;
            newButterflyConfig.randomFlightChance = randomFlightChance;
            newButterflyConfig.numFlybyButterflies = numFlybyButterflies;
            newButterflyConfig.flybyChance = flybyChance;
            landingPoints = applyDraggedPointsToLandingPoints ();
            newButterflyConfig.landingPoints = landingPoints;
            newButterflyConfig.kinectLocation = kinectSensor.transform.position;
            newButterflyConfig.kinectRotation = kinectSensor.transform.localRotation.eulerAngles;
            newButterflyConfig.kinectScale = kinectGuide.transform.localScale.x;
            newButterflyConfig.disperseDestination = disperseDestination;
            newButterflyConfig.disperseDestination2 = disperseDestination2;
            newButterflyConfig.logoScale = logo.transform.localScale.x;
            newButterflyConfig.logoPosition = logo.transform.localPosition;
            return newButterflyConfig;
        }

        public void applyConfigValues (ButterflyConfig butterflyConfig) {
            //applying config values from json. 
            if (butterflyConfig != null) {
                showOutline = butterflyConfig.showOutline;
                outlineEffect.lineThickness = butterflyConfig.lineThickness;
                magicMode = butterflyConfig.magicMode;
                butterflyScale = butterflyConfig.butterflyScale;
                numButterflies = butterflyConfig.numButterflies;
                flightSpeed = butterflyConfig.flightSpeed;
                flappingSpeed = butterflyConfig.flappingSpeed;
                delayToReturn = butterflyConfig.delayToReturn;
                sustainMode = butterflyConfig.sustainMode;
                sustainDelay = butterflyConfig.sustainDelay;
                shyChance = butterflyConfig.shyChance;
                randomFlightChance = butterflyConfig.randomFlightChance;
                numFlybyButterflies = butterflyConfig.numFlybyButterflies;
                flybyChance = butterflyConfig.flybyChance;
                landingPoints = butterflyConfig.landingPoints;
                disperseDestination = butterflyConfig.disperseDestination;
                disperseDestination2 = butterflyConfig.disperseDestination2;
                kinectSensor.transform.position = butterflyConfig.kinectLocation;
                kinectSensor.transform.localRotation = Quaternion.Euler (butterflyConfig.kinectRotation);
                kinectGuide.transform.localScale = new Vector3 (
                    butterflyConfig.kinectScale,
                    butterflyConfig.kinectScale,
                    butterflyConfig.kinectScale
                );

                if (landingPoints == null || landingPoints.Count < 3) {
                    Debug.Log ("Landing points not found in config. Reverting to default.");
                    landingPoints = butterflyConfig.defaultLandingPoints;
                }

                logo.transform.localPosition = butterflyConfig.logoPosition;
                logo.transform.localScale = new Vector3 (
                    butterflyConfig.logoScale,
                    butterflyConfig.logoScale,
                    butterflyConfig.logoScale
                );
            }
        }

        private void sortLandingPoints () {
            //I'm assuming the rock is a simple convex shape.
            //sorts points by x value, left to right. 
            //if there is complex geometry besides a concave, try commenting this out and manually sorting points in json. 
            landingPoints.Sort ((a, b) => a.x.CompareTo (b.x));
        }

        private void checkSustainedPresence () {
            if (sustainMode && Time.time - startedFlyingTime > sustainDelay && !sustained && !landing) {
                startSustainMode ();
            }
        }

        private void checkKeyDown () {
            if (Input.anyKeyDown) {
                if (Input.GetKey ("1") || Input.GetKey ("up")) {
                    //for debugging flying. hold down to simulate presence.  
                    disperse ();
                }
                if (Input.GetKey ("2")) {

                    //forceSustain.
                    if (!sustained) {
                        startSustainMode (); //hold down to simulate extended presence. 

                    }
                }

                if (Input.GetKey ("space")) {
                    reloadScene ();
                }

                if (Input.GetKey ("d")) {
                    toggleDebugMode (); //debug mode for adjusting points and kinect location. 
                }

                if (Input.GetKey ("o")) {
                    toggleOutline ();
                }

                if (Input.GetKey ("k")) {
                    toggleKinectMode ();
                }

                if (Input.GetKey ("p")) {
                    toggleLandingPointVisibility ();
                }

                if (Input.GetKey ("m")) {
                    magicMode = !magicMode;
                    toggleMagicMode (magicMode);
                }

                if (Input.GetKey (KeyCode.KeypadEnter) || Input.GetKey ("enter") || Input.GetKey ("return")) {
                    this.landingPoints = applyDraggedPointsToLandingPoints ();
                    turnOffDebugMode ();
#if UNITY_EDITOR
                    //if in editor, opens popup window.
                    SaveJSONPrompt.Init (this);
#else
                    //skips prompt and just saves a new timestamped file.
                    string butterflyConfigName = "butterfly_production_" + Tools.getTimeStamp () + ".json";
                    saveConfigPath (butterflyConfigName);
                    saveButterflyConfig (butterflyConfigName);
#endif
                }
            }
        }

        public void saveButterflyConfig (string butterflyConfigName) {
            var settings = new Newtonsoft.Json.JsonSerializerSettings ();
            settings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
            settings.Formatting = Formatting.Indented;
            butterflyConfig.defaultLandingPoints = null;
            butterflyConfig.kinectLocation = kinectSensor.transform.position;
            butterflyConfig.kinectRotation = kinectSensor.transform.localRotation.eulerAngles;
            butterflyConfig.kinectScale = kinectGuide.transform.localScale.x;
            string butterflyConfigJSON = JsonConvert.SerializeObject (butterflyConfig, settings);
            File.WriteAllText ("json/" + butterflyConfigName, butterflyConfigJSON);
        }

        public void saveConfigPath (string butterflyConfigName) {
            Config config = new Config ();
            config.config = butterflyConfigName;
            string configJSON = JsonConvert.SerializeObject (config, Formatting.Indented);
            File.WriteAllText ("json/config.json", configJSON);
        }

        private void checkKeyUp () {
            if (Input.GetKeyUp ("1") || Input.GetKeyUp ("up")) {
                if (!Input.GetKey ("2")) {
                    //both buttons should be released in debug sustain mode. 
                    land ();

                }
            }
            if (Input.GetKeyUp ("2")) {
                land ();
            }
        }

        private void setupButterflyInstances () {
            GameObject butterflyContainer = new GameObject ("flybyButterflyContainer");
            butterflyContainer.transform.SetParent (this.transform);
            instanceI = 0;
            butterflies = new List<Butterfly> ();
            for (int i = 0; i < numButterflies; i++) {
                Vector3 randomLandingPoint = getRandomLandingPoint (i);
                Butterfly instance = setupButterflyInstance (butterflyModels[instanceI], randomLandingPoint, i);
                instance.transform.SetParent (butterflyContainer.transform);
                instance.randomFlightChance = randomFlightChance;
                butterflies.Add (instance);
                allRenderers.AddRange (instance.GetComponentsInChildren<Renderer> ());
                instanceI = (int) Mathf.Repeat (i, butterflyModels.Count);
            }
        }

        private void setupFlybyButterflyInstances () {
            GameObject flyByButterflyContainer = new GameObject ("flybyButterflyContainer");
            flyByButterflyContainer.transform.SetParent (this.transform);
            flybyButterflies = new List<Butterfly> ();
            instanceI = 0;
            for (int i = 0; i < numFlybyButterflies; i++) {
                //this origin is temporary and overriden within the butterfly class. 
                Vector3 offScreenOrigin = new Vector3 (-8.0f,
                    2.0f,
                    0.0f
                );
                Butterfly instance = setupButterflyInstance (butterflyModels[instanceI], offScreenOrigin, i);
                instance.transform.SetParent (flyByButterflyContainer.transform);
                instance.flyby = true;
                instance.flybyChance = flybyChance;
                flybyButterflies.Add (instance);
                allRenderers.AddRange (instance.GetComponentsInChildren<Renderer> ());
                instanceI = (int) Mathf.Repeat (i, butterflyModels.Count);
            }
        }

        private void setupKinectGuide () {
            kinectRenderers = new List<Renderer> ();
            kinectRenderers.AddRange (kinectGuide.GetComponentsInChildren<Renderer> ());
            //line renderers are added afterwards.
        }

        private Vector3 getRandomLandingPoint (int i) {
            int originI = (int) Mathf.Repeat (i, landingPoints.Count - 1);
            Vector3 currentOrigin = landingPoints[originI];
            Vector3 nextOrigin = landingPoints[originI + 1];
            return Tools.getRandomPointOnLine (currentOrigin, nextOrigin);
        }

        private Butterfly setupButterflyInstance (GameObject butterflyModel, Vector3 origin, int i) {
            GameObject instance = Instantiate (butterflyModel) as GameObject;
            instance.transform.SetParent (this.transform);
            Butterfly butterflyScript = instance.GetComponent<Butterfly> ();
            if (butterflyScript == null) {
                Debug.Log ("The prefab is missing the butterfly script.");
            } else {
                butterflyScript.origin = origin;
                butterflyScript.scale = butterflyScale;
                butterflyScript.speed = flightSpeed;
                butterflyScript.flapping_speed = flappingSpeed;
                butterflyScript.delayToReturn = delayToReturn;
                butterflyScript.shyChance = shyChance;
                butterflyScript.magicMode = magicMode;
                butterflyScript.disperseDestination = disperseDestination;
                butterflyScript.disperseDestination2 = disperseDestination2;
                butterflyScript.i = i;
            }
            return butterflyScript;
        }

        private void toggleLandingPointVisibility () {
            if (!landingPointVisibility) {
                showDraggableLandingPoints ();
            } else {
                hideDraggableLandingPoints ();
            }
            landingPointVisibility = !landingPointVisibility;
        }

        private void setupDraggablePoints () {
            GameObject draggablePointsContainer = new GameObject ("draggablePointsContainer");
            draggablePointsContainer.transform.SetParent (this.transform);
            draggablePoints = new List<GameObject> ();
            draggablePoints.Clear ();
            foreach (Vector3 point in landingPoints) {
                GameObject instance = Instantiate (landingPointPrefab) as GameObject;
                instance.transform.SetParent (draggablePointsContainer.transform);
                instance.transform.position = point;
                draggablePoints.Add (instance);
            }
        }

        private void hideDraggableLandingPoints () {
            foreach (GameObject point in draggablePoints) {
                point.GetComponent<Renderer> ().enabled = false;
                point.GetComponent<Collider> ().enabled = false; //prevent misclicks,. 
            }
        }

        private void showDraggableLandingPoints () {
            foreach (GameObject point in draggablePoints) {
                point.GetComponent<Renderer> ().enabled = true;
                point.GetComponent<Collider> ().enabled = true; //prevent misclicks,. 
            }
        }

        private void showButterflies () {
            outlineEffect.enabled = showOutline;
            toggleMagicMode (magicMode);
            foreach (Renderer rend in allRenderers) {
                rend.enabled = true;
            }
        }

        private void hideButterflies () {
            outlineEffect.enabled = false;
            toggleMagicMode (false);
            foreach (Renderer rend in allRenderers) {
                rend.enabled = false;
            }
        }

        private void hideKinectGuide () {
            foreach (Renderer rend in kinectRenderers) {
                rend.enabled = false;
            }
        }

        private void showKinectGuide () {
            foreach (Renderer rend in kinectRenderers) {
                rend.enabled = true;
            }
        }

        private void startSustainMode () {
            sustained = true;
            landing = false;
            foreach (Butterfly butterfly in butterflies) {
                butterfly.startSustainMode ();
            }
        }

        private void setupOutline () {
            outlineEffect = cam.GetComponent<cakeslice.OutlineEffect> ();
            if (outlineEffect != null) {
                outlineEffect.enabled = showOutline;
            }
        }

        private void toggleDebugMode () {
            debugMode = !debugMode;
            if (debugMode) {
                //showKinectSkeleton();
                hideButterflies ();
                showKinectGuide ();
                showDraggableLandingPoints ();
                kinectCollider.enabled = false;
#if !UNITY_EDITOR
                Cursor.visible = true;
#endif
            } else {
                this.landingPoints = applyDraggedPointsToLandingPoints ();
                turnOffDebugMode ();
            }
        }

        private void turnOffDebugMode () {
            debugMode = false;
            resetButterfliesToNewLandingPoints ();
            showButterflies ();
            hideDraggableLandingPoints ();
            hideKinectGuide ();
            kinectCollider.enabled = kinectMode;
#if !UNITY_EDITOR
            Cursor.visible = false;
#endif
        }

        private List<Vector3> applyDraggedPointsToLandingPoints () {
            if (Application.isPlaying) {
                this.landingPoints.Clear ();
                foreach (GameObject point in draggablePoints) {
                    this.landingPoints.Add (point.transform.position);
                }
            } else {
                //in editor, default to saved landing points.
                Debug.Log ("This is in editor, default to saved landing points");
            }
            return landingPoints;
        }

        private void resetButterfliesToNewLandingPoints () {
            for (int i = 0; i < numButterflies; i++) {
                Vector3 landingPoint = getRandomLandingPoint (i);
                Butterfly butterfly = butterflies[i];
                butterfly.origin = landingPoint;
                butterfly.reset ();
            }
        }

        private void toggleOutline () {
            if (outlineEffect != null) {
                showOutline = !showOutline;
                outlineEffect.enabled = showOutline;
            }
        }

        private void toggleKinectMode () {
            kinectMode = !kinectMode;
            if (kinectMode) {
                kinectColliderScript.mCollider.enabled = true;
                if (kinectColliderScript.checkIfInBounds ()) {
                    disperse ();
                }
            } else {
                kinectColliderScript.mCollider.enabled = false;
                land ();
            }
        }

        private void toggleMagicMode (bool magicMode) {
            foreach (Butterfly butterfly in butterflies) {
                butterfly.setMagicMode (magicMode);
            }
            foreach (Butterfly butterfly in flybyButterflies) {
                butterfly.setMagicMode (magicMode);
            }
        }

        private void reloadScene () {
            DOTween.Clear (true);
            SceneManager.LoadScene (0);
        }

    }
}