using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Butterflies
{
    public class KinectCollider : MonoBehaviour
    {
        public GameObject colliderObject;
        public ButterflyController butterflyController;
        public Collider mCollider;
        public bool isPresent;
        private float slowUpdateTime = 0.0f;
        private float slowUpdateInterval = 1;
        private bool on;

        // Start is called before the first frame update
        void Start()
        {
            mCollider = GetComponent<Collider>();
            if (!butterflyController.kinectMode)
            {
                mCollider.enabled = false;
            }
        }

        private void Update()
        {
            if (Time.time > slowUpdateTime)
            {
                slowUpdateTime += slowUpdateInterval;
                SlowUpdate();
            }
        }

        private void SlowUpdate()
        {
            if (checkIfInBounds())
            {
                if (!on)
                {
                    butterflyController.disperse();
                    on = true;
                }
            }
            else
            {
                if (on)
                {
                    butterflyController.land();
                    on = false;
                }
            }
        }

        void OnTriggerEnter(Collider collider)
        {
            butterflyController.disperse();
            on = true;
        }

        void OnTriggerExit()
        {
            butterflyController.land();
            on = false;
        }

        public bool checkIfInBounds()
        {
            if (mCollider.bounds.Contains(colliderObject.transform.position))
            {
                return true;
            }
            return false;
        }
    }
}