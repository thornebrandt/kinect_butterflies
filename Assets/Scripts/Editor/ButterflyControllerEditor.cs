#if UNITY_EDITOR

using UnityEngine;
using System.Collections;
using UnityEditor;

namespace Butterflies
{
    [CustomEditor(typeof(ButterflyController))]
    public class ButterflyControllerEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            ButterflyController butterflyController = (ButterflyController)target;
            EditorGUILayout.LabelField("Populates these values with default config. (Happens during playmode automatically)", EditorStyles.boldLabel);
            if (GUILayout.Button("Load Json"))
            {

                butterflyController.loadConfig();
            }

            EditorGUILayout.Space(10);
            EditorGUILayout.LabelField("", GUI.skin.horizontalSlider);
            EditorGUILayout.Space(10);

            DrawDefaultInspector();

            EditorGUILayout.Space(10);
            EditorGUILayout.LabelField("", GUI.skin.horizontalSlider);
            EditorGUILayout.Space(10);
            //example of showing specific field. 
            //butterflyController.numButterflies = EditorGUILayout.IntField("NumButterflies", butterflyController.numButterflies);

            if (GUILayout.Button("Save Json"))
            {
                SaveJSONPrompt.Init(butterflyController);
            }

        }
    }
}

#endif