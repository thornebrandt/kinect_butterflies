using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DragLandingPoint : MonoBehaviour
{

    private Vector3 mouseOffset;
    private float z_position;
    private Material material;
    private Vector3 originalScale;
    private Vector3 rolloverScale;

    void Start()
    {
        material = GetComponent<Renderer>().material;
        originalScale = transform.localScale;
        rolloverScale = originalScale * 2.0f;
    }

    void OnMouseDown()
    {
        mouseOffset = gameObject.transform.position - GetMouseWorldPos();
    }

    void OnMouseEnter()
    {
        material.color = Color.green;
        transform.localScale = rolloverScale;
    }

    void OnMouseExit()
    {
        material.color = Color.white;
        transform.localScale = originalScale;
    }

    void OnMouseDrag()
    {
        transform.position = GetMouseWorldPos() + mouseOffset;
    }

    private Vector3 GetMouseWorldPos()
    {
        Vector3 mousePoint = Input.mousePosition;
        z_position = Camera.main.WorldToScreenPoint(gameObject.transform.position).z;
        mousePoint.z = z_position;
        return Camera.main.ScreenToWorldPoint(mousePoint);
    }
}
