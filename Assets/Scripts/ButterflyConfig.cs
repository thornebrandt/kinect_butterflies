using System;
using System.Collections.Generic;
using UnityEngine;

namespace Butterflies {
    [Serializable]
    public class ButterflyConfig {
        public bool showOutline;
        public float lineThickness; //for outline. 
        public bool magicMode;
        public float butterflyScale;
        public int numButterflies;
        public float flightSpeed;
        public float flappingSpeed;
        public float delayToReturn;
        public bool sustainMode;
        public float sustainDelay;
        public float shyChance;
        public float randomFlightChance;
        public int numFlybyButterflies;
        public float flybyChance;
        public Vector3 kinectLocation;
        public Vector3 kinectRotation;
        public float kinectScale;
        public List<Vector3> landingPoints;
        public List<Vector3> defaultLandingPoints;
        public int screenWidth;
        public int screenHeight;
        public Vector3 disperseDestination;
        public Vector3 disperseDestination2;
        public float logoScale;
        public Vector3 logoPosition;

        public ButterflyConfig () {
            showOutline = false;
            lineThickness = 1.0f;
            magicMode = false;
            butterflyScale = 1;
            numButterflies = 40;
            flightSpeed = 1;
            flappingSpeed = 1;
            delayToReturn = 1;
            sustainDelay = 6;
            shyChance = 0.5f;
            randomFlightChance = 0.001f;
            numFlybyButterflies = 30;
            flybyChance = 0.002f;
            //default landing points, moving away from the editor.
            //this is another place to edit default if you don't want to mess with json.            
            kinectScale = 2;
            screenWidth = 1080;
            screenHeight = 1920;
            kinectLocation = new Vector3 (
                0,
                0, -7.5f
            );
            disperseDestination = new Vector3 (-4.5f,
                6.5f, -1.0f
            );
            disperseDestination2 = new Vector3 (
                4.5f,
                7.0f,
                1.0f
            );
            logoScale = 2.25f;
            logoPosition = new Vector3 (
                0, 2,
                0
            );
            defaultLandingPoints = new List<Vector3> {
                new Vector3 (-3.2f, -1.6f,
                0
                ),
                new Vector3 (-2.5f, -1.18f,
                0
                ),
                new Vector3 (-1.5f, -0.89f,
                0
                ),
                new Vector3 (-0.6f, -0.55f,
                0
                ),
                new Vector3 (
                0,
                0,
                0
                ),
                new Vector3 (
                1.1f,
                0.1f,
                0
                ),
                new Vector3 (
                2.0f, -0.7f,
                0
                ),
                new Vector3 (
                2.5f, -1.3f,
                0
                ),
                new Vector3 (
                3.2f, -1.1f,
                0
                )
            };
        }
    }
}