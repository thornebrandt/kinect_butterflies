using System.Collections.Generic;
using System.IO;
using UnityEngine;

namespace Butterflies {
    public static class Tools {
        public static string getJSON (string path) {
            string filePath = path;
            if (File.Exists (filePath)) {
                using (StreamReader sr = new StreamReader (filePath)) {
                    string line = "";
                    string json = "";
                    int i = 0;
                    while ((line = sr.ReadLine ()) != null) {
                        json += line;
                        i++;
                    }
                    return json;
                }
            }
            Debug.Log ("could not find " + filePath);
            return "";
        }

        public static Vector3 getRandomPointOnLine (Vector3 A, Vector3 B) {
            float max = Vector3.Distance (A, B);
            float x = Random.Range (0.0f, max);
            return x * Vector3.Normalize (B - A) + A;
        }

        public static int getTimeStamp () {
            System.DateTime epochStart = new System.DateTime (1970, 1, 1, 0, 0, 0, System.DateTimeKind.Utc);
            return (int) (System.DateTime.UtcNow - epochStart).TotalSeconds;
        }

        public static Texture2D LoadImage (string filePath) {
            Texture2D tex = null;
            byte[] fileData;
            if (File.Exists (filePath)) {
                fileData = File.ReadAllBytes (filePath);
                tex = new Texture2D (1024, 2048, TextureFormat.Alpha8, false);
                tex.LoadImage (fileData);
            }
            return tex;
        }
    }
}