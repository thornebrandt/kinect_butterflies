#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;
using Newtonsoft.Json;
using System.IO;

namespace Butterflies
{
#if UNITY_EDITOR
    public class SaveJSONPrompt : EditorWindow
    {
        private static string butterflyConfigName;
        private static ButterflyController controller;

        static public void Init(ButterflyController _controller)
        {
            controller = _controller;
            butterflyConfigName = "butterfly" + Tools.getTimeStamp() + ".json";
            SaveJSONPrompt window = ScriptableObject.CreateInstance<SaveJSONPrompt>();
            window.ShowWindow();
        }

        void ShowWindow()
        {
            GetWindow<SaveJSONPrompt>("Save Butterfly Config to JSON");
        }

        void OnGUI()
        {
            EditorGUILayout.Space(10);
            EditorGUILayout.LabelField("Update default build (json/config.json) or save as copy?", EditorStyles.boldLabel);
            EditorGUILayout.Space(10);
            EditorGUILayout.LabelField("", GUI.skin.horizontalSlider);
            EditorGUILayout.Space(10);
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.PrefixLabel("Filename");
            butterflyConfigName = GUILayout.TextField(butterflyConfigName);
            EditorGUILayout.EndHorizontal();
            EditorGUILayout.Space(10);
            EditorGUILayout.LabelField("", GUI.skin.horizontalSlider);
            EditorGUILayout.Space(10);
            EditorGUILayout.BeginHorizontal();
            if (GUILayout.Button("Update Default", EditorStyles.miniButtonLeft, GUILayout.Width(95)))
            {
                controller.saveConfigPath(butterflyConfigName);
                controller.saveButterflyConfig(butterflyConfigName);
                this.Close();
            }
            GUILayout.FlexibleSpace();
            if (GUILayout.Button("Save Copy", EditorStyles.miniButtonLeft, GUILayout.Width(95)))
            {
                controller.saveButterflyConfig(butterflyConfigName);
                this.Close();
            }
            GUILayout.FlexibleSpace();
            if (GUILayout.Button("Cancel", EditorStyles.miniButtonRight, GUILayout.Width(95)))
            {
                this.Close();
            }
            EditorGUILayout.EndHorizontal();

        }
    }
#else   
        public class SaveJSONPrompt : MonoBehaviour {
            static public void Init(ButterflyController controller){

            }
        }

#endif
}
