%YAML 1.1
%TAG !u! tag:unity3d.com,2011:
--- !u!319 &31900000
AvatarMask:
  m_ObjectHideFlags: 0
  m_CorrespondingSourceObject: {fileID: 0}
  m_PrefabInstance: {fileID: 0}
  m_PrefabAsset: {fileID: 0}
  m_Name: butterfly_body_mask
  m_Mask: 01000000010000000100000001000000010000000100000001000000010000000100000001000000010000000100000001000000
  m_Elements:
  - m_Path: 
    m_Weight: 1
  - m_Path: butterfly_position
    m_Weight: 1
  - m_Path: butterfly_position/butterfly_rotation
    m_Weight: 0
  - m_Path: butterfly_position/butterfly_rotation/butterfly_body
    m_Weight: 0
  - m_Path: butterfly_position/butterfly_rotation/butterfly_body/left_wing
    m_Weight: 0
  - m_Path: butterfly_position/butterfly_rotation/butterfly_body/rightwing
    m_Weight: 0
